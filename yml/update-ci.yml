include:
  - local: '/yml/common-ci.yml'

default:
  tags: [ "update-ci" ]

stages:
  - post
  - push
  - build
  - validate
  - deploy

variables:
  CONTAINER_IMAGE: registry.gitlab.com/$CI_PROJECT_PATH
  DOCKER_DRIVER: overlay2
  GIT_SUBMODULE_STRATEGY: recursive
  FORCE_UNSAFE_CONFIGURE: 1
  AWS_BUCKET_PATH: $AWS_BUCKET/$AWS_PROJECT_PATH
  AWS_BRANCHES_PATH: branches
  AWS_BRANCH_PIPE_PATH: $AWS_BRANCHES_PATH/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID
  AWS_TAGS_PATH: tags
  AWS_TAG_PIPE_PATH: $AWS_TAGS_PATH/$CI_COMMIT_REF_NAME/$CI_PIPELINE_ID
  PH_BASE_URL: "https://pvr.pantahub.com"

.update-scheduled:
  stage: post
  variables:
    PH_SCHEDULED_DEVICE: ""
  script:
    - mkdir work; mkdir out; cd work
    - token=$(http --ignore-stdin POST https://api.pantahub.com/auth/login username=$PHUSER password=$PHPASS | jq -r .token)
    # clone target ph device
    - echo "Cloning target device $PH_BASE_URL/$PHUSER/$PH_SCHEDULED_DEVICE"
    - pvr -a $token clone --objects .pvr/objects $PH_BASE_URL/$PHUSER/$PH_SCHEDULED_DEVICE target-device
    - cp -r target-device clean-device
    - cd target-device
    # upgrade apps
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin registry.gitlab.com
    - pvr app ls | while read -r line; do [ -f $line/src.json ] && ( pvr app update $line; pvr app install $line; ) || echo \"Skipping app without src.json $line\"; done
    # commit bsp and app changes
    - pvr add .
    - release_changes=$(pvr diff)
    - pvr commit
    # sign everything
    - mkdir -p ~/.pvr/pvs/
    - wget -c https://gitlab.com/pantacor/pv-developer-ca/-/raw/master/pvs/pvs.defaultkeys.tar.gz -O - | tar xz -C ~/.pvr/
    - pvr sig update
    - pvr add .
    - pvr commit
    # decide whether to continue or not based on changes
    - test -e ../../recipes/$PH_SCHEDULED_DEVICE.json && test "$release_changes" = "{}" && exit 0
    # post changes
    - echo "Posting bsp to $PH_BASE_URL/$PHUSER/$PH_SCHEDULED_DEVICE"
    # XXX: this pvr output was not meant to be used by tools and moved to stderr; hence capturing the stderr
    - post_response=$(pvr -a $token post -m "auto commit from job $CI_PIPELINE_ID" $PH_BASE_URL/$PHUSER/$PH_SCHEDULED_DEVICE 2>&1 | grep Revision)
    - pantahub_revision=$(echo $post_response | sed -e 's/.*Revision \(.*\) (.*/\1/')
    - cd ../../
    # create change log
    - echo $PH_SCHEDULED_DEVICE > out/$PH_SCHEDULED_DEVICE.log
    - jq . <<< $release_changes >> out/$PH_SCHEDULED_DEVICE.log
    # create recipe metadata
    - "jq -n --arg sd \"$PH_SCHEDULED_DEVICE\" --arg pr \"$pantahub_revision\" '{device:$sd, revision:$pr}' > out/$PH_SCHEDULED_DEVICE.json"
  only:
    - schedules
    - web
  artifacts:
    name: out
    paths:
      - out/$PH_SCHEDULED_DEVICE.log
      - out/$PH_SCHEDULED_DEVICE.json

push-metadata:
  stage: push
  script:
    # exit if no post was made
    - test -d out || exit 0
    - mkdir work; cd work
    # clone repo
    - git clone -b $CI_COMMIT_REF_NAME --single-branch https://${PH_CIBOT_GITLAB_USER}:${PH_CIBOT_GITLAB_TOKEN}@$CI_SERVER_HOST/$CI_PROJECT_PATH
    # get change log
    - for log in ../out/*.log; do cat $log >> change.log; done
    # upgrade recipe jsons with results from previous stage
    - cd $CI_PROJECT_NAME
    - mkdir -p recipes; cp ../../out/*.json recipes/
    # commit push
    - git config --global user.name "Pantacor Device Ci"
    - git config --global user.email "${PH_CIBOT_EMAIL:-no-reply@pantace.pantacor.com}"
    - git add recipes
    - git commit -m "auto commit from job $CI_PIPELINE_ID for daily build" -m "$(cat ../change.log)"
    - git push https://${PH_CIBOT_GITLAB_USER}:${PH_CIBOT_GITLAB_TOKEN}@$CI_SERVER_HOST/$CI_PROJECT_PATH HEAD:$CI_COMMIT_REF_NAME
  only:
    - schedules
    - web

.build-stable:
  extends: .mirror-source-code
  stage: build
  variables:
    PH_SCHEDULED_DEVICE: ""
    PH_STABLE_DEVICE: ""
    BUILD: ""
    INSTALLER: "no"
    DEPLOY: "yes"
  script:
    - cd ..
    # get recipe metadata
    - reference_device=$(cat recipes/$PH_SCHEDULED_DEVICE.json | jq -r .device)
    - reference_revision=$(cat recipes/$PH_SCHEDULED_DEVICE.json | jq -r .revision)
    # clone scheduled and stable devices
    - cd work
    - token=$(http --ignore-stdin POST https://api.pantahub.com/auth/login username=$PHUSER password=$PHPASS | jq -r .token)
    - echo "Cloning scheduled device $PH_BASE_URL/$PHUSER/$reference_device/$reference_revision"
    - pvr -a $token clone --objects .pvr/objects $PH_BASE_URL/$PHUSER/$reference_device/$reference_revision reference-device
    - echo "Cloning stable device $PH_BASE_URL/$PHUSER/$PH_STABLE_DEVICE"
    - cd reference-device
    # get build parameters
    - platform=$(cat bsp/build.json | jq -r .platform)
    - target=$(cat bsp/build.json | jq -r .target)
    - buildtarget=$target
    - if test -n "$SUBTARGET"; then buildtarget=$target-$SUBTARGET; platform=$platform-$SUBTARGET; fi
    - pv_manifest_commit=$(cat bsp/build.json | jq -r .commit)
    - pv_manifest_commit=${pv_manifest_commit:-master}
    - pv_manifest_project=$(cat bsp/build.json | jq -r .project)
    - pv_manifest_project=${pv_manifest_project:-pantacor/pv-manifest}
    - _altrepogroups=`cat bsp/build.json | jq -r .altrepogroups`
    - altrepogroups=${ALTREPOGROUPS:-$_altrepogroups}
    - additional_build_options=$(echo $BUILD | jq -r '.[] | select(.name=="default").options')
    - cd ..
    # get source code
    - repo init $REPO_INIT_EXTRA_ARGS $REPO_MIRROR --depth=1 --partial-clone -u https://gitlab.com/${pv_manifest_project} -m release.xml -g runtime,${target},${buildtarget}${altrepogroups:+,${altrepogroups}}
    - cd .repo/manifests/; git fetch origin ${pv_manifest_commit}; git reset --hard ${pv_manifest_commit}; cd -
    - until repo sync --nmu -j10; do echo "Sync failed, retrying..."; done;
    # build with default options
    - echo "Building with default options"
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin registry.gitlab.com
    - build_options="PVR_USE_SRC_BSP=yes PVR_MERGE_SRC=reference-device/.pvr PV_BUILD_INTERACIVE=false $additional_build_options"
    - bash -c "$build_options ./build.docker.sh $buildtarget"
    # post reference device to stable
    - if [ "$DEPLOY" = "yes" ]; then
        cd out/$platform/trail/final/trails/0/;
        echo "Posting updated device to $PH_BASE_URL/$PHUSER/$PH_STABLE_DEVICE (XXX - fixme; we use unstable console output here)";
        post_response=$(pvr -a $token post -m "auto commit for $CI_COMMIT_TAG from job $CI_PIPELINE_ID" $PH_BASE_URL/$PHUSER/$PH_STABLE_DEVICE 2>&1 | grep Revision);
        reference_revision=$(echo $post_response | sed -e 's/.*Revision \(.*\) (.*/\1/');
        reference_device=$PH_STABLE_DEVICE;
        cd -;
      else
        echo "Deploy will not be done";
      fi
    # exit normally if aws was not set
    - test -z "$AWS_BUCKET_PATH" && exit 0
    # prepare img and metadata
    - mkdir pipeline
    - if [ -e out/$platform/*.img ]; then mv out/$platform/*.img pipeline/$PH_STABLE_DEVICE.img; bash ../device-ci/tools/split.sh pipeline/$PH_STABLE_DEVICE.img; img_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE.img)); xz pipeline/$PH_STABLE_DEVICE.img; aws_download_url="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE.img.xz"; fi
    - if [ -d out/$platform/tezi ]; then tar -C out/$platform/tezi -cvf pipeline/$PH_STABLE_DEVICE.tezi.tgz . && tezi_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE.tezi.tgz)) && aws_download_url_tezi="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE.tezi.tgz"; fi
    - if [ -e out/$platform/$platform-rootfs.tar.xz ]; then cp -f out/$platform/$platform-rootfs.tar.xz pipeline/$PH_STABLE_DEVICE.rootfs.tar.xz && rootfs_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE.rootfs.tar.xz)) && aws_download_url_rootfs="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE.rootfs.tar.xz"; fi
    - if [ -e out/$platform/$platform.tar.xz ]; then cp -f out/$platform/$platform.tar.xz pipeline/$PH_STABLE_DEVICE.tar.xz && appengine_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE.tar.xz)) && aws_download_url_appengine="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE.tar.xz"; fi
    - device_nick="$PHUSER/$reference_device"
    - "jq -n --arg jn \"$CI_JOB_NAME\" --arg pl \"$platform\" --arg ta \"$target\" --arg dn \"$device_nick\" --arg pr \"$reference_revision\" --arg op \"$build_options\" --arg au \"$aws_download_url\" --arg is \"$img_sha\" --arg aut \"$aws_download_url_tezi\" --arg ts \"$tezi_sha\" --arg aur \"$aws_download_url_rootfs\" --arg rs \"$rootfs_sha\" --arg aua \"$aws_download_url_appengine\" --arg aps \"$appengine_sha\" '{job:$jn,platform:$pl,target:$ta,devicenick:$dn,pantahubrevision:$pr,images:[{\"name\":\"default\",\"options\":$op,\"url\":$au,\"checksum\":$is,\"tezi_url\":$aut,\"tezi_checksum\":$ts,\"rootfs_url\":$aur,\"rootfs_checksum\":$rs,\"appengine_url\":$aua,\"appengine_checksum\":$aps}]}' > pipeline/$PH_STABLE_DEVICE.json"
    # upload image to aws
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws s3 cp --recursive pipeline/ s3://$AWS_BUCKET_PATH/$AWS_TAG_PIPE_PATH/
    # build with other options
    - build_options_count=$(echo $BUILD | jq length)
    - for (( i=1; i<$build_options_count; i++ )); do
        build_name=$(echo $BUILD | jq -r --argjson in "$i" '.[$in].name');
        echo "Building with $build_name options";
        rm -rf pipeline/$PH_STABLE_DEVICE*.img.xz pipeline/$PH_STABLE_DEVICE*.tgz out/$platform/;
        additional_build_options=$(echo $BUILD | jq -r --argjson in "$i" '.[$in].options');
        build_options="PVR_USE_SRC_BSP=yes PVR_MERGE_SRC=reference-device/.pvr PV_BUILD_INTERACIVE=false $additional_build_options";
        bash -c "$build_options ./build.docker.sh $target";
        img_sha=; aws_download_url=;
        if [ -e out/$platform/*.img ]; then
          mv out/$platform/*.img pipeline/$PH_STABLE_DEVICE-$build_name.img;
          bash ../device-ci/tools/split.sh pipeline/$PH_STABLE_DEVICE-$build_name.img;
          img_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE-$build_name.img));
          xz pipeline/$PH_STABLE_DEVICE-$build_name.img;
          aws_download_url="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE-$build_name.img.xz";
        fi;
        aws_download_url_rootfs=; rootfs_sha=;
        if [ -e out/$platform/$platform-rootfs.tar.xz ]; then
          cp -f out/$platform/$platform-rootfs.tar.xz pipeline/$PH_STABLE_DEVICE-$build_name.rootfs.tar.xz;
          rootfs_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE-$build_name.rootfs.tar.xz));
          aws_download_url_rootfs="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE-$build_name.rootfs.tar.xz";
        fi;
        aws_download_url_tezi=; tezi_sha=;
        if [ -d out/$platform/tezi ]; then
          tar -C out/$platform/tezi/ -cvf pipeline/$PH_STABLE_DEVICE-$build_name.tezi.tgz;
          tezi_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE-$build_name.tezi.tgz));
          aws_download_url_tezi="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE-$build_name.tezi.tgz";
        fi;
        aws_download_url_appengine=; appengine_sha=;
        if [ -e out/$platform/$platform.tar.xz ]; then
          cp -f out/$platform/$platform.tar.xz pipeline/$PH_STABLE_DEVICE.tar.xz;
          appengine_sha=($(sha256sum pipeline/$PH_STABLE_DEVICE.tar.xz));
          aws_download_url_appengine="https://$AWS_BUCKET.s3.amazonaws.com/$AWS_PROJECT_PATH/$AWS_TAG_PIPE_PATH/$PH_STABLE_DEVICE.tar.xz";
        fi;
        jq --arg na "$build_name" --arg op "$build_options" --arg aut "$aws_download_url_tezi" --arg aur "$aws_download_url_rootfs" --arg au "$aws_download_url" --arg is "$img_sha" --arg ts "$tezi_sha" --arg rs "$rootfs_sha" --arg aua "$aws_download_url_appengine" --arg aps "$appengine_sha" '.images += [{"name":$na,"options":$op,"url":$au,"checksum":$is,"rootfs_url":$aur,"rootfs_checksum":$rs,"tezi_url":$aut,"tezi_checksum":$ts,"appengine_url":$aua,"appengine_checksum":$aps}]' pipeline/$PH_STABLE_DEVICE.json > tmp && mv tmp pipeline/$PH_STABLE_DEVICE.json;
        aws s3 cp --recursive pipeline/ s3://$AWS_BUCKET_PATH/$AWS_TAG_PIPE_PATH/;
      done
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - work/pipeline/$PH_STABLE_DEVICE.json

deploy-stable:
  stage: deploy
  variables:
    TAG_STATUS: "stable"
  script:
    - if [[ "$CI_COMMIT_TAG" == *-rc* ]]; then TAG_STATUS="release-candidate"; elif [[ "$CI_COMMIT_TAG" == *-* ]]; then TAG_STATUS=`echo "$CI_COMMIT_TAG" | rev | cut -f1 -d- | rev`; fi
    # exit if aws was not set
    - test -z AWS_BUCKET_PATH && exit 0
    # download tags metadata
    - cd work
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws s3 cp s3://$AWS_BUCKET_PATH/stable.json stable.json || echo "{\"release-candidate\":[],\"stable\":[]}" > stable.json
    # prepare pipeline metadata
    - git_describe=$(git describe --tags --always)
    - commit_time=$(git show -s --format=%ci $CI_COMMIT_SHA)
    - pvr_version=$(pvr -v)
    - "jq -n --arg se \"$CI_SERVER_URL\" --arg pj \"$CI_PROJECT_PATH\" --arg br \"$CI_COMMIT_REF_NAME\" --arg cm \"$CI_COMMIT_SHA\" --arg gd \"$git_describe\" --arg pl \"$CI_PIPELINE_ID\" --arg ct \"$commit_time\" --arg pv \"$pvr_version\" '{server: $se, project: $pj, ref: $br, commit: $cm, gitdescribe: $gd, pipeline: $pl, committime:$ct, pvrversion:$pv}' > pipeline.json"
    # upload pipeline metadata to aws
    - aws s3 cp pipeline.json s3://$AWS_BUCKET_PATH/$AWS_TAG_PIPE_PATH/pipeline.json
    # prepare tags metadata
    - for target in pipeline/*.json; do jq --argjson tg "$(<$target)" '.devices += [$tg]' pipeline.json > tmp && mv tmp pipeline.json ; done
    - "jq --arg ts \"$TAG_STATUS\" --argjson pl \"$(<pipeline.json)\" '.[$ts] |= [$pl] + .' stable.json > tmp && mv tmp stable.json"
    # upload metadata to aws
    - aws s3 cp stable.json s3://$AWS_BUCKET_PATH/stable.json
    # trigger docs pipeline
    - "test -z \"$DEPLOY_TRIGGER_PROJECT\" || curl -X POST -F token=\"$DEPLOY_TRIGGER_TOKEN\" -F ref=master https://gitlab.com/api/v4/projects/$DEPLOY_TRIGGER_PROJECT/trigger/pipeline"
  only:
    - tags
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - work/pipeline.json
      - work/stable.json
